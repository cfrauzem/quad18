// Analog voltage reading
// Voltage divider factor
#define FAC (2.2/(2.2+10.0))
#define THRESH 3.5

// Analog reading variables
double CELL1;
double CELL2;
double CELL3;
double CELL1_2;
double CELL1_2_3;

// ANALOG READING FUNCTION
double sampleVoltage(int ch, bool convert) {
  int an_reading;
  double an_volt;
  double vIN;

  // 10-bit resolution
  // Value Range:   0 to (2^10)-1 i.e. 0 to 1023
  // Voltage Range: 0 to 4.94V
  an_reading = analogRead(ch);
  an_volt = (4.94/1024.00)*an_reading;
  if(convert) {
    vIN = an_volt/FAC;
  }
  else {
    vIN = an_volt;
  }
  
  Serial.print("ch");   Serial.print(ch);   Serial.print(": ");
  Serial.print(vIN);    Serial.print("V");  Serial.print("\t");

  return vIN;
}

// INITIALIZE LEDS FUNCTION
void initLED(uint8_t type) {
  for(int i=2; i<10; i++)
  {
    pinMode(i, type);
  }
}

// TOGGLE LEDS FUNCTIONS
void toggleLED(uint8_t state) {
  for(int i=2; i<10; i++)
  {
    digitalWrite(i, state);
  }
}


void setup() {
  Serial.begin(115200);
  initLED(OUTPUT);
}


void loop() {
  // Analog reading sample multiple channels
  CELL1     = sampleVoltage(0, false);
  CELL1_2   = sampleVoltage(1, true);
  CELL1_2_3 = sampleVoltage(2, true);
  Serial.print("\n");

  // Calculate cell voltages
  CELL2 = CELL1_2 - CELL1;
  CELL3 = CELL1_2_3 - CELL1_2;

  Serial.print("1: "); Serial.print(CELL1); Serial.print("V\t");
  Serial.print("2: "); Serial.print(CELL2); Serial.print("V\t");
  Serial.print("3: "); Serial.print(CELL3); Serial.print("V\t");
  Serial.print("\n");
  
  // If cell voltage is less than threshold:
  if (CELL1 < THRESH || CELL2 < THRESH || CELL3 < THRESH) {
    // Turn on LEDs
    toggleLED(HIGH);
    delay(250);
  }
  
  // Turn off LEDs
  toggleLED(LOW);

  delay(250);
}
