// Analog voltage reading
// Voltage divider factor
#define FAC (2.197/(2.197+9.996))

// ANALOG READING FUNCTION
double sampleVoltage(int ch, bool convert) {
  int an_reading;
  double an_volt;
  double vIN;

  // 10-bit resolution
  // Value Range:   0 to (2^10)-1 i.e. 0 to 1023
  // Voltage Range: 0 to 5V
  an_reading = analogRead(ch);
  
  // Resolution: 4.9 mV
  // ATmega 38P datasheet: 5V/1024bits =  0.004883 V/but
  // 5V/1023bits = 0.004888 V/bit
  an_volt = ( (5.0-0.0)/(1023.0-0.0) )*(an_reading-0.0) + 0.0;

  // Voltage divider conversion
  if(convert) {
    vIN = an_volt/FAC;
  }
  else {
    vIN = an_volt;
  }
  
  Serial.print("ch");         Serial.print(ch);         Serial.print(": ");
  Serial.print(an_reading);   Serial.print("bits, ");
  Serial.print(an_volt);      Serial.print("V, ");        
  Serial.print(vIN);          Serial.print("V");        Serial.print("\t");

  return vIN;
}


void setup() {
  // put your setup code here, to run once:
  Serial.begin(115200);
}


void loop() {
  // put your main code here, to run repeatedly:
  sampleVoltage(0, false);
  sampleVoltage(1, true);
  sampleVoltage(2, true);
  Serial.print("\n");
  
  delay(2000);
}
