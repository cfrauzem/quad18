// ---------------------------------------3---------------------------------------
// Alternative 3:
// - Use functions to further reduce/simplify Alternative 2
// Pro: less lines of code, improved readability

// HIGH and LOW are just aliases for the numbers 1 and 0 respectively
// i.e. uint8_t HIGH = 1, uint8_t LOW = 0
// digitalWrite expects the pin and value to be uint8_t
// -> you can pass LOW or HIGH to functions as uint8_t 0 or 1
// The same goes for INPUT, OUTPUT, RISING, FALLING, etc.
//void initLED(uint8_t type) {
//  for(int i=2; i<10; i++)
//  {
//  	pinMode(i, type);
//  }
//}

//void toggleLED(uint8_t state) {
//  for(int i=2; i<10; i++)
//  {
//  	digitalWrite(i, state);
//  }
//}
// -------------------------------------------------------------------------------

// ---------------------------------------4---------------------------------------
// Alternative 4: bitmath
// Output pin
#define PIN_LED1 PD2 // PD2 -> 2, Arduino pin 2

// Input pin
#define PIN_IN PB5 // PB5 -> 5, Arduino pin 13

// Bit shift operator
// Left shift operator <<:
// These operators cause the bits in the left operand to be shifted left or right 
// by the number of positions specified by the right operand
// When you shift a value x by y bits (x << y), 
// the leftmost y bits in x are lost, literally shifted out of existence:
#define LED1 (1<<PIN_LED1)	// i.e. 00000001 -> 00000100
#define LEDIN (1<<PIN_IN)   // i.e. 00000001 -> 00100000

// Bitwise OR
// Operates on each bit position independently
// The bitwise OR of two bits is 1 if either or both of the input bits is 1, otherwise it is 0
// In other words:
// 0 | 0 == 0
// 0 | 1 == 1
// 1 | 0 == 1
// 1 | 1 == 1

// Assignment operators
// Shorthand: x = x + 1 -> x += 1

// PORTD |= LED1 -> PORTD = PORTD | LED1 with LED1 = 00000100
// Summary: only bit 2 is changed to 1, if it was 0, 0|1=1, if it was 1 1|1=1
//	      : all other remain the same, if it was 0, 0|0=0, if it was 1, 1|0=1
#define LED1_ON (PORTD |= LED1)

// Bitwise AND
// Operates on each bit position independently
// If both input bits are 1, the resulting output is 1, otherwise the output is 0
// In other words:
// 0 & 0 == 0
// 0 & 1 == 0
// 1 & 0 == 0
// 1 & 1 == 1

// Bitwise NOT
// Applied to a single operand to its right
// i.e. need to use parentheses to flip entire number if not stored as variable first
// ~00000001 = 10000001, ~(00000001) = 11111110
// Bitwise NOT changes each bit to its opposite: 0 becomes 1, and 1 becomes 0

// PORTD &= ~LED1 -> PORTD = PORTD & ~LED1 with LED1 = 00000100
// ~LED = 11111011
// Summary: only bit 2 is changed to 0
#define LED1_OFF (PORTD &= ~LED1)

// Read input
// PINB contains 0/1 input information of all PORTB pins -> need to isolate PB5
// PINB & LEDIN -> PINB & (1<<PB5) -> PINB & 00100000
// Summary: all bits except bit 5 are 0
//        : bit 5 is 1 if 1 or 0 if 0
//        : two outcomes, (1) 00000000 or (2) 00100000 i.e. false or true
#define READ_LEDIN (PINB & LEDIN)
// -------------------------------------------------------------------------------


void setup() {
// ---------------------------------------1---------------------------------------
// Alternative 1: 
// - Separate line to initialize each LED
// - Pro: can personalize each command
// - Con: tedious
//  pinMode(2, OUTPUT);
//  pinMode(3, OUTPUT);
//  pinMode(4, OUTPUT);
//  pinMode(5, OUTPUT);
//  pinMode(6, OUTPUT);
//  pinMode(7, OUTPUT);
//  pinMode(8, OUTPUT);
//  pinMode(9, OUTPUT);
// -------------------------------------------------------------------------------

// ---------------------------------------2---------------------------------------
// Alternative 2
// - For-loop to initialize LEDs
// - Pro: less lines of code
// - Con: need to use switch or if-else statements to personalize commands
//  for(int i=2; i<10; i++)
//  {
//  	pinMode(i, OUTPUT);
//  }
// -------------------------------------------------------------------------------

// ---------------------------------------3---------------------------------------
// Alternative 3
//  // OUPUT = 1
//  initLED(1);
// -------------------------------------------------------------------------------  

// ---------------------------------------4---------------------------------------
// Alternative 4: bitmath
  // Set all pins of PORTD as inputs (default)
  // DDRD = 0xff; would set all pins of PORTD as outputs
  // 0x00: hex (prefix 0x) for binary (prefix 0b) 0b00000000
  // 0xff: hex for 0b1111111
  DDRD = 0x00;
  
  // Set all pins of PORTB as inputs (default)
  // -> PIN_IN is already set as input
  DDRB = 0x00;
  
  // Set LED1 i.e. pin 2 i.e PD2 as output
  // Use data direction register
  // DDRD |= LED1 -> DDRD |= (1<<PIN_LED1) -> DDRD = DDRD | (1<<2)
  // Summary: only bit position 2 is altered and set high i.e. output
  DDRD |= LED1;
  
  // Set all pins of PORTD low (default)
  // PORTD = 0xff; would set all pins of PORTD high
  PORTD = 0x00;
  
  // Enable internal pull-up resistor for input pin
  // If PORTxn is written to '1' when the pin is configured as an input pin, 
  // the pull-up resistor is activated
  // To switch the pull-up resistor off,
  // PORTxn has to be written to '0' or the pin has to be configured as an output pin
  PORTB |= LEDIN;
// -------------------------------------------------------------------------------

Serial.begin(9600);
}


void loop() {
// ---------------------------------------4---------------------------------------
// Alternative 4: Bitmath
// **********Output Example**********
//  // Turn on LED1
//  LED1_ON;
//  
//  delay(500);
//  
//  // Turn off LED1
//  LED1_OFF;
//  
//  delay(500);
// **********************************

// **********Input Example**********
// Pulling Arduino pin 13 low will turn on LED
//if(READ_LEDIN) {
//    Serial.println("HIGH");
//    LED1_OFF;
//}
//else {
//  Serial.println("LOW");
//  LED1_ON;
//}
//
//  delay(500);
// **********************************
// -------------------------------------------------------------------------------

// ---------------------------------------3---------------------------------------
// Alternative 3
//  // LOW = 0, HIGH = 1
//  toggleLED(1);
//  delay(1000);
//  toggleLED(0);
//  delay(1000);
// -------------------------------------------------------------------------------

// ---------------------------------------2---------------------------------------
// Alternative 2
// - For-loop to drive pins high or low
//  for(int i=2; i<10; i++)
//  {
//  	digitalWrite(i, HIGH);
//  }
  
//  delay(1000);
  
//  for(int i=2; i<10; i++)
//  {
//  	digitalWrite(i, LOW);
//  }
  
//  delay(1000);
// -------------------------------------------------------------------------------

// ---------------------------------------1---------------------------------------
// Alternative 1
// - Separate line to drive each pin HIGH or LOW
//  digitalWrite(2, HIGH);
//  digitalWrite(3, HIGH);
//  digitalWrite(4, HIGH);
//  digitalWrite(5, HIGH);
//  digitalWrite(6, HIGH);
//  digitalWrite(7, HIGH);
//  digitalWrite(8, HIGH);
//  digitalWrite(9, HIGH);
  
//  delay(1000);
  
//  digitalWrite(2, LOW);
//  digitalWrite(3, LOW);
//  digitalWrite(4, LOW);
//  digitalWrite(5, LOW);
//  digitalWrite(6, LOW);
//  digitalWrite(7, LOW);
//  digitalWrite(8, LOW);
//  digitalWrite(9, LOW);
//  digitalWrite(13,LOW);
  
//  delay(1000);
// -------------------------------------------------------------------------------
}
