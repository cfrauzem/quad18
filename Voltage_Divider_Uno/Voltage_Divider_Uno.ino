// Analog voltage reading
// Voltage divider factor
#define FAC (2.2/(2.2+10.0))

// Analog reading variable declaration
int an_reading;
double an_volt;
double vIN;

// ANALOG READING FUNCTION
// - Eliminates redundat code when sampling multiple channels
void sampleVoltage(int ch, bool convert) {
  // 10-bit resolution
  // Value Range:   0 to (2^10)-1 i.e. 0 to 1023
  // Voltage Range: 0 to 5V
  // an_volt = [(5V-0V)/(1023-0)]*(an_reading-0) + 0V
  an_reading = analogRead(ch);
  an_volt = ( (5.0-0.0)/(1023.0-0.0) )*(an_reading-0.0) + 0.0;
  if(convert) {
    vIN = an_volt/FAC;
  }
  else {
    vIN = an_volt;
  }
  
  Serial.print("ch: ");
  Serial.println(ch);
  
  Serial.print("analog reading: ");
  Serial.println(an_reading);
  Serial.print("analog_voltage: "); 
  Serial.println(an_volt);
  Serial.print("vIN: "); 
  Serial.println(vIN);
}


void setup() {
  Serial.begin(115200);
  
}


void loop() {
  // Analog readingL sample multiple channels
  sampleVoltage(0, false);
  sampleVoltage(1, true);
  sampleVoltage(2, true);
}
