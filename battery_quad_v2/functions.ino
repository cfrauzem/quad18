//******************************START FUNCTIONS*******************************
// ATMEGA: double == float
// ATMEGA: int == uint16_t
// -> for-loops: can use uint8_t to save memory space


// RUNNING AVG FUNCTION
void runningAvg(double *cells) {
  bool      convert[3] = {0, 1, 1};
  uint32_t  timeStart;

  // Reset voltage array
  for(uint8_t c=0; c<3; c++) {
    cells[c] = 0.0;
  }

  // Sum voltage readings COUNT times
  for(uint8_t i=0; i<COUNT; i++) {
    // Time stamp
    timeStart = micros();

//    // Print time stamp
//    Serial.print(timeStart/1.0e6);  Serial.print("\t");

    for(uint8_t c=0; c<3; c++) {
      cells[c] += sampleVoltage(c, convert[c]);
    }
    
//    // Line break
//    Serial.print("\n");
    
    // Delay to ensure proper frequency
    // Ensure LEDs are up-to-date
    while( !( micros()-timeStart>(1.0e6/(long)FREQ) ) ) {
      updateLED();
    }
  }

  // Average reading i.e. SUM/COUNT
  for(uint8_t c=0; c<3; c++) {
    cells[c] /= COUNT;
  }
}


// ANALOG READING FUNCTION
double sampleVoltage(uint8_t ch, bool convert) {
  uint16_t  an_reading;
  double    an_volt;
  double    vIN;
  
  // 10-bit resolution
  // Value Range:   0 to (2^10)-1 i.e. 0 to 1023
  // Voltage Range: 0 to 4.94V
  an_reading = analogRead(ch);
  an_volt = (VREF/BITRES)*an_reading;
  
  if(convert) {
    vIN = an_volt/FAC;
  }
  else {
    vIN = an_volt;
  }
  
//  Serial.print("ch");       Serial.print(ch);   Serial.print(":\t");
//  Serial.print(vIN);        Serial.print("V");  Serial.print("\t");

  return vIN;
}


void updateLED(void) {
// Flash LEDs
    // Flash LEDs at LEDInterval without delay()
    if(millis() - LEDLastUpdate > LEDInterval && tripped) {
      
      // Save the last time you blinked the LED 
      LEDLastUpdate = millis();
      
      // If LEDs are off, turn on and vice-versa:
      if (LEDState == LOW) {
        LEDState = HIGH;
      }
      else{
        LEDState = LOW;
      }
    }
    // If cell voltage is still nominal:
    else if (!tripped) {
      LEDState = HIGH;
    }
    
    // Update LEDs
    toggleLED(LEDState);
}


// TOGGLE LEDS FUNCTIONS
void toggleLED(uint8_t state) {
  for(int i=2; i<10; i++)
  {
    digitalWrite(i, state);
  }
}


// INITIALIZE LEDS FUNCTION
void initLED(void) {
  for(int i=2; i<10; i++)
  {
    pinMode(i, OUTPUT);
  }

  // LED start-up sequence
  // Ensure LEDs are on at end
  for(int l=0; l<5; l++) {
    toggleLED(LOW);
    delay(50);
    toggleLED(HIGH);
    delay(50);
  }
}


//*******************************END FUNCTIONS*********************************
