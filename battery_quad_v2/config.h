//******************************START PARAMETERS******************************


// Debug
//#define DEBUG

// Voltage divider factor
#define FAC (2.2/(2.2+10.0))

// Cell voltage correction factors
#define CELL2_CORRECTION 0.1

// Reference voltage
// f indicates float, ATMEGA: float == double
#define VREF    4.94f

// ADC resolution
// 10-bit resolution
#define BITRES  1024.0f

// LiPo voltage lower threshhold
#define THRESH            3.5f
#define WATCHDOG_THRESH   10

// Running average
#define COUNT 10       // Count
#define FREQ  100      // Sample frequency

// Cell voltage variables
bool      tripped  = false;
uint8_t   watchdog = 0;

// Counter
uint16_t counter = 0;
  
// LEDs
uint8_t   LEDState      = HIGH;
uint32_t  LEDLastUpdate = 0;
uint32_t  LEDInterval   = 250;


//*******************************END PARAMETERS*******************************
