// BATTERY QUAD
// LiPo battery analog voltage reading


// Include header file
#include "config.h"


void setup() {
  // Initialize Serial communication
  #ifdef DEBUG
    Serial.begin(115200);
  #endif
  
  initLED();
}


void loop() {
  // Cell voltage array
  double cells[3];

  // Wire voltages
  double WIRE1;       // white wire
  double WIRE1_2;     // yellow wire
  double WIRE1_2_3;   // red wire

  double CELL1;      
  double CELL2;
  double CELL3;
  
  // Calculate average voltage
  // Sample count: COUNT
  // Sample frequency: FREQ
  runningAvg(cells);

  // Assign average voltages
  WIRE1     = cells[0];
  WIRE1_2   = cells[1];
  WIRE1_2_3 = cells[2];

//  // Print voltages
//  Serial.print("1:\t");   Serial.print(CELL1);    Serial.print("V\t");
//  Serial.print("12:\t");  Serial.print(CELL1_2);   Serial.print("V\t");
//  Serial.print("123:\t"); Serial.print(CELL1_2_3);  Serial.print("V\t");
//  Serial.print("\n");

  // Calculate cell voltages
  CELL1 = WIRE1;
  CELL2 = WIRE1_2 - WIRE1 + CELL2_CORRECTION;
  CELL3 = WIRE1_2_3 - WIRE1_2;

  #ifdef DEBUG
    Serial.print(counter);  Serial.print("\t");
    Serial.print("1:\t");   Serial.print(CELL1); Serial.print("V\t");
    Serial.print("2:\t");   Serial.print(CELL2); Serial.print("V\t");
    Serial.print("3:\t");   Serial.print(CELL3); Serial.print("V\t");
    Serial.print(watchdog); 
    Serial.print("\n");
  #endif
  
  // If cell voltage is less than threshold:
  // Increment watchdog counter
  if ( (CELL1 < THRESH || CELL2 < THRESH || CELL3 < THRESH) && !tripped) {    // Caution: logical order of precedence
    watchdog++;
  }
  // Else:
  // Reset watchdog only while tripped = false
  // i.e. after initial trip, tripped = true regardless of cell voltage
  // X-consecutive readings below sensor will cause trip
  // -> To restore additive method previously used, comment out else if
  else if (!tripped) {
    watchdog = 0;
  }

  // Given x-consecutive voltage readings below the voltage threshhold:
  // Activate the alert
  if(watchdog>=WATCHDOG_THRESH) {
    tripped = true;
  }
//  else {
//    tripped = false;
//  }
  counter++;
}
