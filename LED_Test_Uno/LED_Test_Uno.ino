// INITIALIZE LEDS FUNCTION
// HIGH and LOW are just aliases for the numbers 1 and 0 respectively
// i.e. uint8_t HIGH = 1, uint8_t LOW = 0
// digitalWrite expects the pin and value to be uint8_t
// -> you can pass LOW or HIGH to functions as uint8_t 0 or 1
// The same goes for INPUT, OUTPUT, RISING, FALLING, etc.
void initLED(uint8_t type) {
  for(int i=2; i<10; i++)
  {
    pinMode(i, type);
  }
}

// TOGGLE LEDS FUNCTIONS
void toggleLED(uint8_t state) {
  for(int i=2; i<10; i++)
  {
    digitalWrite(i, state);
  }
}

void setup() {
  Serial.begin(115200);
  initLED(OUTPUT);
}


void loop() {
  // Turn on LEDs
  toggleLED(HIGH);

  delay(1000);

  // Turn off LEDs
  toggleLED(LOW);

  delay(1000);
}
